import React from 'react';
import Auth from "./components/Admin/Auth/Auth";
import {Redirect, Route} from "react-router-dom";
import {connect} from "react-redux";
import Main from "./components/Admin/Main/Main";

function App(props) {

    return (
        <div>
            <Route exact path="/">
                {props.isLogin ? <Redirect to="/auth/main"/> : <Auth/>}
            </Route>
            <Route path="/auth/main" component={Main}/>
        </div>
    );
}

function mapDispatchToProps(state) {
    return {
        isLogin: state.isLogin
    }
}

export default connect(mapDispatchToProps, null)(App);