import React, {useEffect, useState} from 'react';
import axios from "axios";
import {BASE_URL} from "../Const/Const";
import {Table} from 'antd';

const {Column} = Table;


function Products() {

    const [productList, setProductList] = useState([]);
    const [loading, setLoading] = useState(true);
    useEffect(() => {

        axios.post(BASE_URL + "/variations", {
                size: 100,
                page: "all",
                stock: {
                    exist: true,
                    location: [
                        42
                    ]
                }
            },
            {
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token"),
                    "content-type": "application/json"
                }
            }
        )
            .then(response => {
                setProductList(response.data.items);
                setLoading(false);
            }).catch(error => {
            console.log(error);
        });
    }, []);

    const data = [];
    productList.map((item) => {
        for (let i = 0; i < 1; i++) {
            let newProduct = {
                productName: item.importRecord.productName,
                supplierName: item.importRecord.supplierName,
                sellPrice: item.stocks[i].sellPrice.UZS,
                lastUpdateTime: item.lastUpdateTime
            };
            data.push(newProduct);
        }
    });


    return (
        <div className="all-product-list">
            <Table dataSource={data} loading={loading} locale={{emptyText: " "}}>
                <Column title="Name" dataIndex="productName" key="name"/>
                <Column title="Supplier" dataIndex="supplierName" key="supplier"/>
                <Column title="Price" dataIndex="sellPrice" key="price"/>
                <Column title="Data update" dataIndex="lastUpdateTime" key="address"/>
            </Table>
        </div>
    );
}

export default Products;