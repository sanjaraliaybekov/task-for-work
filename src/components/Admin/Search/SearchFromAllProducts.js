import React, {useState, useEffect} from "react";
import {Table, Input, Button, Space} from 'antd';
import Highlighter from 'react-highlight-words';
import {SearchOutlined} from '@ant-design/icons';
import axios from "axios";
import {BASE_URL} from "../Const/Const";


function SearchFromAllProducts() {

    const [productList, setProductList] = useState([]);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const [loading, setLoading] = useState(true);
    useEffect(() => {

        axios.post(BASE_URL + "/variations", {
                size: 100,
                page: "all",
                stock: {
                    exist: true,
                    location: [
                        42
                    ]
                }
            },
            {
                headers: {
                    "Authorization": "Bearer " + localStorage.getItem("token"),
                    "content-type": "application/json"
                }
            }
        )
            .then(response => {
                setProductList(response.data.items);
                setLoading(false);
            }).catch(error => {
        });
    }, []);

    const data = [];

    productList.map((item) => {
        for (let i = 0; i < 1; i++) {
            let newProduct = {
                productName: item.importRecord.productName,
                supplierName: item.importRecord.supplierName,
                sellPrice: item.stocks[i].sellPrice.UZS,
                lastUpdateTime: item.lastUpdateTime
            };
            data.push(newProduct);
        }
    });

    function getColumnSearchProps(dataIndex) {
        return {
            filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
                <div style={{padding: 8}}>
                    <Input
                        placeholder={`Search ${dataIndex}`}
                        value={selectedKeys[0]}
                        onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                        onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        style={{width: 188, marginBottom: 8, display: 'block'}}
                    />
                    <Space>
                        <Button
                            type="primary"
                            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                            icon={<SearchOutlined/>}
                            size="small"
                            style={{width: 90}}
                        >
                            Search
                        </Button>
                        <Button onClick={() => handleReset(clearFilters)} size="small" style={{width: 90}}>
                            Reset
                        </Button>
                    </Space>
                </div>
            ),
            filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
            onFilter: (value, record) =>
                record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
            onFilterDropdownVisibleChange: visible => {
                if (visible) {
                }
            },
            render: text =>
                searchedColumn === dataIndex ? (
                    <Highlighter
                        highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
                        searchWords={[searchText]}
                        autoEscape
                        textToHighlight={text.toString()}
                    />
                ) : (
                    text
                ),
        }
    }


    function handleSearch(selectedKeys, confirm, dataIndex) {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    }

    function handleReset(clearFilters) {
        clearFilters();
        setSearchText('');
    }

    const columns = [
        {
            title: 'Name',
            dataIndex: 'productName',
            key: 'name',
            sortDirections: ['descend', 'ascend'],
            sorter: (a, b) => a.supplierName.length - b.supplierName.length,
            ...getColumnSearchProps('productName'),
        },
        {
            title: 'Supplier',
            dataIndex: 'supplierName',
            key: 'supplier',
            sortDirections: ['descend', 'ascend'],
            sorter: (a, b) => a.supplierName.length - b.supplierName.length,
            ...getColumnSearchProps('supplierName'),
        },
        {
            title: 'Price',
            dataIndex: 'sellPrice',
            key: 'Price',
            sortDirections: ['descend', 'ascend'],
            sorter: (a, b) => a.sellPrice > b.sellPrice,
            ...getColumnSearchProps('sellPrice'),
        },
        {
            title: 'Data update',
            dataIndex: 'lastUpdateTime',
            key: "dataUpdate",
            sortDirections: ['descend', 'ascend'],
            sorter: (a, b) => a.lastUpdateTime.substring(0, 3) > b.lastUpdateTime.substring(0, 3),
            ...getColumnSearchProps('lastUpdateTime'),
        },
    ];

    return <Table columns={columns} dataSource={data}
                  locale={{emptyText: " "}} loading={loading}/>;
}

export default SearchFromAllProducts;