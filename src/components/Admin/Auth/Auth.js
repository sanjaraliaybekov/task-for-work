import React, {useState} from 'react';
import {Form, Input, Button} from 'antd';
import './auth.scss';
import axios from "axios";
import {BASE_URL, SIGN_IN_ACTION} from "../Const/Const";
import {connect} from "react-redux";
import {message} from 'antd';

function Auth(props) {

    const [authUser, setAuthUser] = useState({_username: "", _password: "", _subdomain: ""});

    function getUserInfo(e) {
        setAuthUser({
            ...authUser,
            [e.target.name]: e.target.value
        });
    }

    function logIn() {

        let formData = new FormData();
        formData.append('_username', authUser._username);
        formData.append('_password', authUser._password);
        formData.append('_subdomain', authUser._subdomain);

        axios.post(BASE_URL + "/security/auth_check", formData,
            {headers: {'content-type': 'multipart/form-data'}})
            .then(response => {
                if (response.status !== 200) {
                    props.signIn(false);
                } else {
                    localStorage.setItem("token", response.data.token);
                    props.signIn(true);
                    const success = () => {
                        message.success('Siz tizimga muvaffaqiyatli kirdingiz!');
                    };
                    success();
                }
            }).catch(error => {
            const errorText = () => {
                message.error('Foydalanuvchi aniqlanmadi! Iltimos Ma\'lumotlaringizni qaytadan tekshirib ko\'ring!');
            };
            errorText();
            console.log(error);
        });
    }

    return (
        <div className="admin-auth">
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center">
                        <div className="content">
                            <Form id="myForm"
                                  name="myForm"
                                  labelCol={{
                                      span: 8,
                                  }}
                                  wrapperCol={{
                                      span: 16,
                                  }}
                                  initialValues={{
                                      remember: true,
                                  }}
                                  autoComplete="off"
                            >
                                <Form.Item
                                    label="Username"
                                    name="_username"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your username!',
                                        },
                                    ]}
                                >
                                    <Input name="_username" onChange={getUserInfo}/>
                                </Form.Item>

                                <Form.Item
                                    label="Password"
                                    name="_password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input your password!',
                                        },
                                    ]}
                                >
                                    <Input.Password name="_password" onChange={getUserInfo}/>
                                </Form.Item>
                                <Form.Item
                                    label="SubDomain"
                                    name="_subdomain"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Please input subdomain!',
                                        },
                                    ]}
                                >
                                    <Input name="_subdomain" onChange={getUserInfo}/>
                                </Form.Item>
                                <Form.Item
                                    wrapperCol={{
                                        offset: 8,
                                        span: 16,
                                    }}
                                >
                                    <Button type="primary" htmlType="submit" onClick={logIn}>
                                        Submit
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

function mapDispatchToProps(dispatch) {
    return {
        signIn: (isLogin) => {
            return dispatch({
                type: SIGN_IN_ACTION,
                payload: isLogin
            })
        }
    }
}

export default connect(null, mapDispatchToProps)(Auth);