import React from 'react';
import {Layout, Menu, Breadcrumb} from 'antd';
import {UserOutlined, SearchOutlined, LogoutOutlined} from '@ant-design/icons';
import './main.scss';
import {Link, Route, Switch} from "react-router-dom";
import Products from "../Products/Products";
import SearchFromAllProducts from "../Search/SearchFromAllProducts";

const {SubMenu} = Menu;
const {Header, Content, Footer, Sider} = Layout;

function Main() {

    function logOut() {
        localStorage.removeItem("token");
        window.location.reload();
    }

    return (
        <div className="admin-main">
            <Layout>
                <Header className="header">
                    <div className="logo"/>
                    <Menu theme="dark" mode="horizontal"
                          defaultSelectedKeys={['1']}>
                        <Menu.Item key="1">Home</Menu.Item>
                        <Menu.Item key="2" style={{position: "absolute", right: "50px"}} onClick={logOut}><Link
                            className="d-flex justify-content-center align-items-center"
                            to="/">Log Out<LogoutOutlined style={{marginLeft: "8px"}}/></Link>
                        </Menu.Item>
                    </Menu>
                </Header>
                <Content style={{padding: '0 50px'}}>
                    <Breadcrumb style={{margin: '16px 0'}}>
                        <Breadcrumb.Item onClick={logOut}><Link
                            to="/">Login</Link></Breadcrumb.Item>
                        <Breadcrumb.Item><Link to="/auth/main">Main</Link></Breadcrumb.Item>
                    </Breadcrumb>
                    <Layout className="site-layout-background" style={{padding: '24px 0'}}>
                        <Sider className="site-layout-background" width={200}>
                            <Menu
                                mode="inline"
                                style={{height: '100%'}}
                            >
                                <SubMenu key="products" icon={<UserOutlined/>} title="Products">
                                    <Menu.Item key="1">
                                        <Link to="/auth/main/all-products">Products List</Link>
                                    </Menu.Item>
                                </SubMenu>
                                <SubMenu key="search" icon={<SearchOutlined/>} title="Search Product">
                                    <Menu.Item key="2"><Link to="/auth/main/search-from-all-products">Search
                                        Products</Link></Menu.Item>
                                </SubMenu>
                                <p className="text-center d-flex justify-content-center align-items-center position-absolute bottom-0 my-name"><UserOutlined className="mx-2"/>Created by SanjarAli</p>
                            </Menu>
                        </Sider>
                        <Content style={{padding: '0 24px', minHeight: 280}}>
                            <Switch>
                                <Route path="/auth/main/all-products" component={Products}/>
                                <Route path="/auth/main/search-from-all-products" component={SearchFromAllProducts}/>
                            </Switch>
                        </Content>
                    </Layout>
                </Content>
            </Layout>
        </div>
    );
}

export default Main;