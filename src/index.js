import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from "./App";
import './Globall.scss';
import 'antd/dist/antd.css';
import {BrowserRouter} from "react-router-dom";
import {createStore} from "redux";
import {SignInReducer} from "./components/Admin/Reducer/SignInReducer";
import {Provider} from "react-redux";

const store = createStore(SignInReducer, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter>
            <Provider store={store}>
                <App/>
            </Provider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);

